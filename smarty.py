from datetime import date
import json
import os
from pathlib import Path
import subprocess

import typer
from xdg import xdg_config_home, xdg_data_home


APP_NAME = "smarty"
CONFIG_FILE_NAME = "config.json"


app = typer.Typer()


def main():
    app()


def get_config_dir() -> Path:
    try:
        return Path(os.environ["SMARTY_CONFIG_DIR"])
    except KeyError:
        return xdg_config_home() / APP_NAME


def get_data_dir() -> Path:
    try:
        return Path(os.environ["SMARTY_DATA_DIR"])
    except KeyError:
        return xdg_data_home() / APP_NAME


def init_config_dir() -> None:
    config_dir = get_config_dir()
    config_dir.mkdir(exist_ok=True, parents=True)
    config_file = config_dir / CONFIG_FILE_NAME
    if not config_file.exists():
        print(f"Creating default config file {config_file}")
        default_config = {
            "drives": [],
        }
        with open(config_file, mode="w") as fh:
            json.dump(default_config, fh)


def init_data_dir() -> None:
    data_dir = get_data_dir()
    if not data_dir.exists():
        print(f"Creating data directory {data_dir}")
        data_dir.mkdir(parents=True)


def load_drives():
    with open(get_config_dir() / CONFIG_FILE_NAME) as fh:
        config = json.load(fh)
    return config["drives"]


class DriveNotConfiguredError(Exception):
    pass


def load_drive(nickname: str):
    drives = load_drives()
    for drive in drives:
        if drive["nickname"] == nickname:
            return drive
    raise DriveNotConfiguredError(nickname)


def device_path(drive_id: str) -> Path:
    return Path(f"/dev/disk/by-id/{drive_id}")


def make_report_stem(report_date: date) -> str:
    return report_date.strftime("%Y_%m_%d")


def parse_date(report_path: Path) -> str:
    year, month, day = report_path.stem.split("_")
    return date(int(year), int(month), int(day))


def find_all_report_dates(drive) -> list[date]:
    report_dir = get_data_dir() / drive['id']
    return [parse_date(path) for path in report_dir.glob("*.json")]


def find_latest_report_date(drive: dict) -> date:
    all_dates = sorted(find_all_report_dates(drive))

    if len(all_dates) == 0:
        return None

    return all_dates[-1]


def find_prev_report_date(drive: dict) -> date:
    all_dates = sorted(find_all_report_dates(drive))

    if len(all_dates) < 2:
        return None

    return all_dates[-2]


def is_attached(drive) -> bool:
    return device_path(drive["id"]).exists()


def import_smart(report_path: Path, drive_type: str) -> dict:
    with open(report_path) as fh:
        report = json.load(fh)

    if drive_type == "nvme":
        section = report["nvme_smart_health_information_log"]
        return {attrib.lower(): section[attrib] for attrib in section}

    if drive_type == "ata":
        smart = {}
        for row in report["ata_smart_attributes"]["table"]:
            smart[row["name"].lower()] = {
                "id": row["id"],
                "value": row["value"],
                "threshold": row["thresh"],
                "raw_value": row["raw"]["string"],
            }
        return smart

    raise RuntimeError(f"Unhandled drive_type: {drive_type}")


@app.callback()
def init_app():
    """
    Monitor the SMART data for your hard drives.
    """
    init_config_dir()
    init_data_dir()


@app.command()
def status():
    drives = load_drives()

    columns = [
        {
            "name": "Latest Report",
            "width": 13,
            "func": find_latest_report_date,
        },
        {
            "name": "Attached",
            "width": 8,
            "func": is_attached,
        },
        {
            "name": "Device Nickname",
            "width": 32,
            "func": lambda drive: drive["nickname"],
        },
    ]

    headers = []
    for column in columns:
        name = column["name"]
        width = column["width"]
        headers.append(f"{name:{width}}")
    print("  ".join(headers))

    dividers = []
    for column in columns:
        width = column["width"]
        dividers.append("-" * width)
    print("  ".join(dividers))

    for drive in drives:
        values = []
        for column in columns:
            value = str(column["func"](drive))
            width = column["width"]
            values.append(f"{value:{width}}")
        print("  ".join(values))


@app.command()
def scan(nickname: str):
    try:
        drive = load_drive(nickname)
    except DriveNotConfiguredError:
        print(f"A drive named '{nickname}' isn't found in the config file. Aborting.")
        return

    if not is_attached(drive):
        print("Drive isn't attached. Aborting.")
        return

    device = device_path(drive["id"])
    report_dir = get_data_dir() / drive["id"]
    report_dir.mkdir(exist_ok=True)
    report_date = date.today()
    report_stem = make_report_stem(report_date)

    print(f"Creating reports for {device} ...")

    report_path = f"{report_dir}/{report_stem}.txt"
    with open(report_path, mode="w") as fh:
        proc = subprocess.run(
            ["sudo", "smartctl", "--all", str(device)],
            stdout=fh,
            check=True,
        )
    print(f"... [DONE] {report_path}")

    report_path = f"{report_dir}/{report_stem}.json"
    with open(report_path, mode="w") as fh:
        proc = subprocess.run(
            ["sudo", "smartctl", "--all", "--json", str(device)],
            stdout=fh,
            check=True,
        )
    print(f"... [DONE] {report_path}")

    if drive["type"] == "ata":
        smart = import_smart(report_path, drive["type"])

        prev_report_date = find_prev_report_date(drive)
        if prev_report_date is not None:
            prev_report_path = f"{report_dir}/{make_report_stem(prev_report_date)}.json"
            prev_smart = import_smart(prev_report_path, drive["type"])

        print()
        print("Selected SMART values:")

        if "reallocated_sector_ct" in smart:
            print("  Reallocated sectors:")
            print(f"    {report_date}: {smart['reallocated_sector_ct']['value']}")
            if prev_report_date is not None:
                print(f"    {prev_report_date}: {prev_smart['reallocated_sector_ct']['value']}")

        if "reported_uncorrect" in smart:
            print("  Reported uncorrectable errors:")
            print(f"    {report_date}: {smart['reported_uncorrect']['value']}")
            if prev_report_date is not None:
                print(f"    {prev_report_date}: {prev_smart['reported_uncorrect']['value']}")

        if "command_timeout" in smart:
            print("  Command timeout:")
            print(f"    {report_date}: {smart['command_timeout']['value']}")
            if prev_report_date is not None:
                print(f"    {prev_report_date}: {prev_smart['command_timeout']['value']}")

        if "current_pending_sector" in smart:
            print("  Current pending sector count:")
            print(f"    {report_date}: {smart['current_pending_sector']['value']}")
            if prev_report_date is not None:
                print(f"    {prev_report_date}: {prev_smart['current_pending_sector']['value']}")

        if "offline_uncorrectable" in smart:
            print("  Uncorrectable sector count:")
            print(f"    {report_date}: {smart['offline_uncorrectable']['value']}")
            if prev_report_date is not None:
                print(f"    {prev_report_date}: {prev_smart['offline_uncorrectable']['value']}")


@app.command()
def stats(nickname: str, attribute: str):
    try:
        drive = load_drive(nickname)
    except DriveNotConfiguredError:
        print(f"A drive named '{nickname}' isn't found in the config file. Aborting.")
        return

    data = []
    report_dir = get_data_dir() / drive["id"]
    for report_path in report_dir.glob("*.json"):
        smart = import_smart(report_path, drive["type"])
        smart["date"] = parse_date(report_path)
        data.append(smart)

    for row in sorted(data, key=lambda r: r["date"]):
        print(row["date"], row[attribute.lower()])


if __name__ == "__main__":
    main()
